<?php

namespace Dideo\DataartLaravel;

use Dideo\DataartLaravel\Objects\Action;
use Dideo\DataartLaravel\Objects\ActionList;
use Dideo\DataartLaravel\Objects\User;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

/**
 * Base model to communicate with DataArt servers
 *
 */
class Dataart
{
	private $apiKey;

	private $baseUrl = 'https://sourcing.datartproject.com/';
	private $identifyUserUrl = 'users/identify';
	private $sendActionsUrl = 'events/send-actions';
	private $requestTimeout = 2;
	private $connectionTimeout = 1;

	private $client;

	private const REQUEST_KEYS = ['API_key' => 'API_key'];

	public function __construct(string $apiKey, ?int $requestTimeout = null, ?int $connectionTimeout = null) {
		$this->apiKey = $apiKey;
		$this->requestTimeout = $requestTimeout ?? $this->requestTimeout;
		$this->connectionTimeout = $connectionTimeout ?? $this->connectionTimeout;
	}

    /**
     * get or start request client
     *
     * @return Client
     */
    private function getClient(): Client {
        if (isset($this->client)) {
            return $this->client;
        }
        $this->client =
            new Client(
                [
                    'base_uri' => $this->baseUrl,
                    RequestOptions::TIMEOUT => $this->requestTimeout,
                    RequestOptions::CONNECT_TIMEOUT => $this->connectionTimeout,
                    RequestOptions::HEADERS =>
                        [
                            self::REQUEST_KEYS['API_key'] => $this->apiKey,
//                        'content-type' => 'application/json',
                        ]
                ]
            );

        return $this->client;
    }


    /**
     * identify new user
     *
     * @param User $user
     * @return string|null
     * @throws Exception
     */
    public function identifyUser(User $user): ?string {
        $url = $this->identifyUserUrl;
        $data = $user->toArray();
        return $this->postRequest($url, $data);
    }

    /**
     * send list of actions
     *
     * @param ActionList $actionList
     * @return string|null
     * @throws Exception
     */
    public function sendActions(ActionList $actionList): ?string {
        $url = $this->sendActionsUrl;
        $data = $actionList->toArray();
        return $this->postRequest($url, $data);
    }

    /**
     * send an action
     *
     * @param Action      $action
     * @param string|null $date
     * @return string|null
     * @throws GuzzleException
     */
    public function sendAction(Action $action, ?string $date=null): ?string {
        $actionList = new ActionList([$action], $date);
        $url = $this->sendActionsUrl;
        $data = $actionList->toArray();
        return $this->postRequest($url, $data);
    }


    /**
     * send a post request to dataArt
     *
     * @param string $url
     * @param array  $data
     * @return string|null
     * @throws GuzzleException
     */
    private function postRequest(string $url, array $data): ?string {
        $client = $this->getClient();
        $response = null;

        if (isset($client)) {
            try {
                $response = $client->post($url, [
                        RequestOptions::JSON => $data
                    ]
                )->getBody();

            } catch (Exception $exception) {
                throw $exception;
            }
        }
        return $response;
    }

}
