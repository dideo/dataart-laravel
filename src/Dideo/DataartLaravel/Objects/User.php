<?php

namespace Dideo\DataartLaravel\Objects;

class User implements DataartObject
{
	private $userKey;
	private $metadata;

	private const REQUEST_KEYS = ['user_key' => 'user_key', 'metadata' => 'metadata'];

	public function __construct(string $userKey, array $metadata = []) {
		$this->userKey = $userKey;
		$this->metadata = $metadata;
	}

	public function toArray(): array {
		return [
            self::REQUEST_KEYS['user_key'] => $this->userKey,
            self::REQUEST_KEYS['metadata'] => $this->metadata
        ];
    }
}
