<?php

namespace Dideo\DataartLaravel\Objects;

class ActionList implements DataartObject
{
	private $date;
	private $actions;

	private const REQUEST_KEYS = ['timestamp' => 'timestamp', 'actions' => 'actions'];

	public function __construct(array $actions = [], ?string $date = null) {
		$this->date = $date ?? date(DATE_ATOM);
		$this->actions = $actions;
	}

	public function toArray(): array {
		$data = [
            self::REQUEST_KEYS['timestamp'] => $this->date,
            self::REQUEST_KEYS['actions'] => []
        ];
        $actions = $this->actions;
        foreach ($actions as $action) {
            if ($action instanceof Action) {
                $data[self::REQUEST_KEYS['actions']][] = $action->toArray();
            }
        }
        return $data;
    }
}
