<?php

namespace Dideo\DataartLaravel\Objects;

interface DataartObject
{
    public function toArray():array;
}
