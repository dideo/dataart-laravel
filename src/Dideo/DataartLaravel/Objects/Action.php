<?php

namespace Dideo\DataartLaravel\Objects;

class Action implements DataartObject
{
	private $actionIdentifier, $userKey, $actionDate;
	private $actionMetadata;
	private $isAnonymousUser;

	private const SEND_ACTION_REQUEST_KEYS = ['user_key' => 'user_key', 'metadata' => 'metadata', 'timestamp' => 'timestamp',
		'key' => 'key', 'is_anonymous_user' => 'is_anonymous_user'];

	public function __construct(string  $actionIdentifier, string $userKey, bool $isAnonymousUser = false,
	                            ?string $actionDate = null, array $metadata = []) {
		$this->actionDate = $actionDate ?? date(DATE_ATOM);
		$this->actionIdentifier = $actionIdentifier;
		$this->userKey = $userKey;
		$this->isAnonymousUser = $isAnonymousUser;
        $this->actionMetadata = $metadata;
    }

    public function toArray(): array {
        return [
            self::SEND_ACTION_REQUEST_KEYS['key'] => $this->actionIdentifier,
            self::SEND_ACTION_REQUEST_KEYS['user_key'] => $this->userKey,
            self::SEND_ACTION_REQUEST_KEYS['is_anonymous_user'] => $this->isAnonymousUser,
            self::SEND_ACTION_REQUEST_KEYS['timestamp'] => $this->actionDate,
            self::SEND_ACTION_REQUEST_KEYS['metadata'] => $this->actionMetadata
        ];
    }
}
